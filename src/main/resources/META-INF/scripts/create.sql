create sequence HIBERNATE_SEQUENCE start with 10 increment by 10 no cycle;

CREATE TABLE BOOK (
    ID BIGINT NOT NULL,
    TITLE VARCHAR(255),
    PRICE FLOAT,
    DESCRIPTION VARCHAR(255),
    ISBN VARCHAR(255),
    NBOFPAGE INTEGER,
    ILLUSTRATIONS SMALLINT DEFAULT 0,
    PRIMARY KEY (ID)
);

create table tags (
    book_id bigint not null,
    tag varchar(100)
);

create table AUTHOR (
    ID BIGINT NOT NULL,
    FIRST_NAME varchar(255),
    LAST_NAME varchar(255),
    PHONE varchar(255),
    ADDRESS varchar(255),
    PRIMARY KEY (ID)
);

create table AUTHOR_P (
    ID BIGINT NOT NULL,
    FIRST_NAME varchar(255),
    LAST_NAME varchar(255),
    PHONE varchar(255),
    PRIMARY KEY (ID)
);

create table AUTHOR_A (
    ID BIGINT NOT NULL,
    FIRST_NAME varchar(255),
    LAST_NAME varchar(255),
    ADDRESS varchar(255),
    PRIMARY KEY (ID)
);


create table BOOKS_AUTHORS (
    AUTHOR_ID BIGINT NOT NULL,
    BOOK_ID BIGINT NOT NULL
);