package ru.bell.beans;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerFactoryFactory implements FactoryBean<EntityManagerFactory>, InitializingBean {

    private String persistenceUnitName;

    @Override
    public EntityManagerFactory getObject() throws Exception {
        return Persistence.createEntityManagerFactory(this.persistenceUnitName);
    }

    @Override
    public Class<?> getObjectType() {
        return EntityManagerFactory.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void setPersistenceUnitName(String persistenceUnitName) {
        this.persistenceUnitName = persistenceUnitName;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("initializing EntityManagerFactoryFactory");
    }
}
