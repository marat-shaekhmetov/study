package ru.bell.beans;

import org.springframework.beans.factory.annotation.Autowired;
import ru.bell.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class JpaChecker {

    @Autowired
    private EntityManager entityManager;

    public void run(){
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();

        Book book = new Book("H2G2", "WorkBook1", 12.5F, "1-84023-742-2", 354, false);

        Set<String> tags = new LinkedHashSet<>();
        tags.add("tag1");
        tags.add("tag2");
        tags.add("tag3");
        book.setTags(tags);

        Author author1 = new Author(new PersonalRequisites("FIRST1", "LAST1"));
        AuthorWithPhone author2 = new AuthorWithPhone(new PersonalRequisites("FIRST2", "LAST2"), "+7901");
        AuthorWithAddress author3 = new AuthorWithAddress(new PersonalRequisites("FIRST3", "LAST3"), "Ufa, Sverdlova str, 92");
        book.setAuthorList(Arrays.asList(author1, author2, author3));
        System.out.println("persisting book");


        entityManager.persist(book);

        /*
        контекст постоянства сбросится после коммита
         */
        tx.commit();

        /*
         контекст снова заполнится после выполнения запроса по поиску всех книг
         */
        List<Book> bookList = entityManager.createNamedQuery("findAllBooks", Book.class).getResultList();
        System.out.println("-------CHECK GET BOOKS WITH JPA ------------");
        for (Book fetchedBook: bookList) {
            System.out.println(String.format("found book: id: %d, title: %s, description: %s", fetchedBook.getId(),
                    fetchedBook.getTitle(), fetchedBook.getDescription()));
        }

        /*
        очистим контекст постоянства
         */
        entityManager.clear();

        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Book> criteriaQuery = criteriaBuilder.createQuery(Book.class);
        Root<Book> root = criteriaQuery.from(Book.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("title"), "H2G2"));
        System.out.println("----loading book via criteria api, tags should not be loaded");
        Book bookViaCriteria = entityManager.createQuery(criteriaQuery).getSingleResult();
        System.out.println("----now tags should be loaded with lazy load");
        Set<String> loadedTags = bookViaCriteria.getTags();
        System.out.println("----result: tags: " + loadedTags.stream().collect(Collectors.joining(", ")));

        entityManager.close();

        testViaJdbc();
    }

    /**
     * Проверка работы через JDBC
     */
    private void testViaJdbc() {
        try (Connection connection = DriverManager.getConnection("jdbc:derby:memory:tutorial")) {
            System.out.println("-------CHECK GET BOOKS WITH JDBC ------------");
            Statement sqlStatement = connection.createStatement();
            PreparedStatement getTags = connection.prepareStatement("select book_id, tag from tags where book_id = ?");
            PreparedStatement getAuthors = connection.prepareStatement("select author_id from BOOKS_AUTHORS where book_id = ?");
            PreparedStatement getAuthor = connection.prepareStatement("select FIRST_NAME, LAST_NAME from author where id = ?" +
                    "union select FIRST_NAME, LAST_NAME from author_a where id = ?" +
                    "union select FIRST_NAME, LAST_NAME from author_p where id = ?");

            ResultSet resultSet = sqlStatement.executeQuery("select id, title, description from Book");
            while (resultSet.next()) {
                System.out.println(String.format("found book: id: %d, title: %s, description: %s", resultSet.getInt("id"),
                        resultSet.getString("title"), resultSet.getString("description")));
                /*
                    getting tags
                 */
                getTags.setInt(1, resultSet.getInt("id"));
                ResultSet tags = getTags.executeQuery();
                StringBuilder tagsBuilder = new StringBuilder();
                Set<String> tagSet = new HashSet<>();
                while (tags.next()) {
                    tagSet.add(tags.getString("tag"));
                }
                tags.close();
                if (!tagSet.isEmpty()) {
                    System.out.println("\ttags: " + tagSet.stream().collect(Collectors.joining(", ")));
                }

                /*
                 getting authors
                 */

                getAuthors.setInt(1, resultSet.getInt("id"));
                ResultSet authors = getAuthors.executeQuery();
                List<Author> authorList = new ArrayList<>();
                while (authors.next()) {
                    getAuthor.setInt(1, authors.getInt( "author_id"));
                    getAuthor.setInt(2, authors.getInt( "author_id"));
                    getAuthor.setInt(3, authors.getInt( "author_id"));
                    ResultSet authorResult = getAuthor.executeQuery();
                    authorResult.next();
                    authorList.add(new Author(new PersonalRequisites(authorResult.getString("FIRST_NAME"),
                            authorResult.getString("LAST_NAME"))));
                }
                authors.close();
                if (!authorList.isEmpty()) {
                    System.out.println("\tauthors: " + authorList.stream().map(a -> a.getPersonalRequisites().getFirstName()
                            + " " + a.getPersonalRequisites().getLastName()).collect(Collectors.joining(", ")));
                }
            }
            sqlStatement.close();
            getTags.close();
            getAuthors.close();

            resultSet.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
