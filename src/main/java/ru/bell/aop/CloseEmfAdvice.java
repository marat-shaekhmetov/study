package ru.bell.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import javax.persistence.EntityManagerFactory;

public class CloseEmfAdvice implements MethodInterceptor {

    private EntityManagerFactory entityManagerFactory;

    public CloseEmfAdvice(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        try {
            return invocation.proceed();
        } finally {
            System.out.println("closing EMF");
            entityManagerFactory.close();
        }
    }
}
