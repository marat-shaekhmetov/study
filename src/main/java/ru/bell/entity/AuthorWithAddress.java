package ru.bell.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "AUTHOR_A")
public class AuthorWithAddress extends Author {

    @NotNull
    private String address;

    public AuthorWithAddress(String address) {
        this.address = address;
    }

    public AuthorWithAddress(PersonalRequisites personalRequisites, String address) {
        super(personalRequisites);
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
