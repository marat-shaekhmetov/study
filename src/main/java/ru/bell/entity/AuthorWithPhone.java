package ru.bell.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "AUTHOR_P")
public class AuthorWithPhone extends Author {
    private String phone;

    public AuthorWithPhone() {
    }

    public AuthorWithPhone(PersonalRequisites personalRequisites, String phone) {
        super(personalRequisites);
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
