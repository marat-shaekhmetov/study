package ru.bell.entity;

import javax.persistence.*;

@Entity
@Table(name = "AUTHOR")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Author {

    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    private PersonalRequisites personalRequisites;

    public Author() {
    }

    public Author(PersonalRequisites personalRequisites) {
        this.personalRequisites = personalRequisites;
    }

    public PersonalRequisites getPersonalRequisites() {
        return personalRequisites;
    }

    public void setPersonalRequisites(PersonalRequisites personalRequisites) {
        this.personalRequisites = personalRequisites;
    }
}