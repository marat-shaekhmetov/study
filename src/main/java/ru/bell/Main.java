package ru.bell;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.bell.beans.JpaChecker;

public class Main {

    public static void main(String[] args) {
        ApplicationContext appContext = new GenericXmlApplicationContext("/META-INF/context.xml");
        JpaChecker jpaChecker = appContext.getBean("jpaCheckerBean", JpaChecker.class);
        jpaChecker.run();
    }
}
